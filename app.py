from time import sleep
import canvas

def draw_point(x, y, color):
    canvas.set_fill_color(*color)
    canvas.fill_pixel(x, y)

def draw_line(x1, y1, x2, y2, color):
    canvas.set_stroke_color(*color)
    canvas.draw_line(x1, y1, x2, y2)

class Model:
    def __init__(self):
        self.polygons = []
        self.x = self.y = self.z = 0
        
    def add_polygon(self, polygon):
        self.polygons.append(polygon)
        
    def get_polygon_point(self, point):
        return (self.x + point[0], self.y + point[1], self.z + point[2])

class Stage:
    def __init__(self, width, height):
        self.camera_width = 2
        self.camera_height = 2
        self.screen_width = width
        self.screen_height = height
        self.camera = (0.0, 0.0, -2.0)
        self.models = []
        
    def add_model(self, model):
        self.models.append(model)
                        
    def draw(self):
        canvas.set_size(self.screen_width, self.screen_height)
        canvas.set_stroke_color(255, 255, 255)
        canvas.draw_rect(0, 0, self.screen_width, self.screen_height)
        
        canvas.begin_updates()
        for model in self.models:
            for polygon in model.polygons:
                for i in range(0, len(polygon)):
                    x1, y1 = self._project_world_to_screen(model.get_polygon_point(polygon[i]))
                    x2, y2 = self._project_world_to_screen(model.get_polygon_point(polygon[(i+1) % len(polygon)]))
                    
                    draw_line(x1, y1, x2, y2, (255, 0, 255))
        canvas.end_updates()
        
    def _project_world_to_screen(self, point):
        point_x, point_y, point_z = point
        camera_x, camera_y, camera_z = self.camera
        
        dz = point_z - camera_z if point_z != camera_z else 1
        x = ((point_x - camera_x) / dz + (self.camera_width / 2)) / self.camera_width * self.screen_width
        y = ((point_y - camera_y) / dz + (self.camera_height / 2)) / self.camera_height * self.screen_height
        return x, y

def build_cube():
    model = Model()
    faces = [
        [ # Back face
            (-0.5, 0.5, 0.5),
            (0.5, 0.5, 0.5),
            (0.5, -0.5, 0.5),
            (-0.5, -0.5, 0.5)
        ],
        [ # Front face
            (-0.5, 0.5, -0.5),
            (0.5, 0.5, -0.5),
            (0.5, -0.5, -0.5),
            (-0.5, -0.5, -0.5)
        ],
        [ # Left face
            (-0.5, 0.5, 0.5),
            (-0.5, 0.5, -0.5),
            (-0.5, -0.5, -0.5),
            (-0.5, -0.5, 0.5)
        ],
        [ # Right face
            (0.5, 0.5, 0.5),
            (0.5, 0.5, -0.5),
            (0.5, -0.5, -0.5),
            (0.5, -0.5, 0.5)
        ],
        [ # Top face
            (-0.5, 0.5, 0.5),
            (0.5, 0.5, 0.5),
            (0.5, 0.5, -0.5),
            (-0.5, 0.5, -0.5)
        ],
        [ # Bottom face
            (-0.5, -0.5, 0.5),
            (0.5, -0.5, 0.5),
            (0.5, -0.5, -0.5),
            (-0.5, -0.5, -0.5)
        ]
    ]
    
    for face in faces:
        model.add_polygon(face)

    return model

def main():
    WIDTH = 256
    HEIGHT = 256

    stage = Stage(WIDTH, HEIGHT)
    model = build_cube()
    stage.add_model(model)

    for x in range(-100, 100, 2):
        model.x = x / 100.0
        stage.draw()
        sleep(0.01)

if __name__ == '__main__':
    main()
